EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Connector:Conn_01x03_Female J1
U 1 1 6298D67C
P 1550 1400
F 0 "J1" H 1700 1500 50  0000 C CNN
F 1 "8-102202-4" H 1650 1650 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x03_P2.54mm_Vertical" H 1550 1400 50  0001 C CNN
F 3 "~" H 1550 1400 50  0001 C CNN
F 4 "A32559-ND" H 1500 1750 50  0000 C CNN "DigiKey"
	1    1550 1400
	-1   0    0    -1  
$EndComp
$Comp
L Device:C_Small C1
U 1 1 6298EF40
P 2000 1400
F 0 "C1" H 2092 1491 50  0000 L CNN
F 1 "10nF 100V" H 1800 1200 50  0000 L CNN
F 2 "Capacitor_SMD:C_1206_3216Metric_Pad1.33x1.80mm_HandSolder" H 2000 1400 50  0001 C CNN
F 3 "~" H 2000 1400 50  0001 C CNN
F 4 "399-17911-1-ND" H 1550 1100 50  0000 L CNN "DigiKey"
	1    2000 1400
	1    0    0    -1  
$EndComp
Wire Wire Line
	1750 1500 2000 1500
Wire Wire Line
	2000 1500 2500 1500
Connection ~ 2000 1500
Wire Wire Line
	2000 1300 1750 1300
Wire Wire Line
	1750 1400 1850 1400
Wire Wire Line
	1850 1400 1850 1200
Wire Wire Line
	1850 1200 2500 1200
$Comp
L Connector:Conn_01x03_Female J3
U 1 1 629914D1
P 3300 1400
F 0 "J3" H 3450 1500 50  0000 C CNN
F 1 "8-102202-4" H 3400 1650 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x03_P2.54mm_Vertical" H 3300 1400 50  0001 C CNN
F 3 "~" H 3300 1400 50  0001 C CNN
F 4 "A32559-ND" H 3300 1400 50  0001 C CNN "DigiKey"
	1    3300 1400
	-1   0    0    -1  
$EndComp
$Comp
L Device:C_Small C3
U 1 1 6299153B
P 3750 1400
F 0 "C3" H 3842 1446 50  0000 L CNN
F 1 "10nF 100V" H 3550 1200 50  0000 L CNN
F 2 "Capacitor_SMD:C_1206_3216Metric_Pad1.33x1.80mm_HandSolder" H 3750 1400 50  0001 C CNN
F 3 "~" H 3750 1400 50  0001 C CNN
F 4 "399-17911-1-ND" H 3750 1400 50  0001 C CNN "DigiKey"
	1    3750 1400
	1    0    0    -1  
$EndComp
Wire Wire Line
	3500 1500 3750 1500
Wire Wire Line
	3750 1500 4250 1500
Connection ~ 3750 1500
Wire Wire Line
	3750 1300 3500 1300
Wire Wire Line
	3500 1400 3600 1400
Wire Wire Line
	3600 1400 3600 1200
Wire Wire Line
	3600 1200 4250 1200
$Comp
L Connector:Conn_01x03_Female J2
U 1 1 62992EE0
P 1550 2150
F 0 "J2" H 1700 2250 50  0000 C CNN
F 1 "8-102202-4" H 1650 2400 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x03_P2.54mm_Vertical" H 1550 2150 50  0001 C CNN
F 3 "~" H 1550 2150 50  0001 C CNN
F 4 "A32559-ND" H 1550 2150 50  0001 C CNN "DigiKey"
	1    1550 2150
	-1   0    0    -1  
$EndComp
$Comp
L Device:C_Small C2
U 1 1 62992F92
P 2000 2150
F 0 "C2" H 2092 2196 50  0000 L CNN
F 1 "10nF 100V" H 1850 1950 50  0000 L CNN
F 2 "Capacitor_SMD:C_1206_3216Metric_Pad1.33x1.80mm_HandSolder" H 2000 2150 50  0001 C CNN
F 3 "~" H 2000 2150 50  0001 C CNN
F 4 "399-17911-1-ND" H 2000 2150 50  0001 C CNN "DigiKey"
	1    2000 2150
	1    0    0    -1  
$EndComp
Wire Wire Line
	1750 2250 2000 2250
Wire Wire Line
	2000 2250 2500 2250
Connection ~ 2000 2250
Wire Wire Line
	2000 2050 1750 2050
Wire Wire Line
	1750 2150 1850 2150
Wire Wire Line
	1850 2150 1850 1950
Wire Wire Line
	1850 1950 2500 1950
$Comp
L Connector:Conn_01x03_Female J4
U 1 1 62992FAD
P 3300 2150
F 0 "J4" H 3450 2250 50  0000 C CNN
F 1 "8-102202-4" H 3400 2400 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x03_P2.54mm_Vertical" H 3300 2150 50  0001 C CNN
F 3 "~" H 3300 2150 50  0001 C CNN
F 4 "A32559-ND" H 3300 2150 50  0001 C CNN "DigiKey"
	1    3300 2150
	-1   0    0    -1  
$EndComp
$Comp
L Device:C_Small C4
U 1 1 62992FB7
P 3750 2150
F 0 "C4" H 3842 2196 50  0000 L CNN
F 1 "10nF 100V" H 3600 1950 50  0000 L CNN
F 2 "Capacitor_SMD:C_1206_3216Metric_Pad1.33x1.80mm_HandSolder" H 3750 2150 50  0001 C CNN
F 3 "~" H 3750 2150 50  0001 C CNN
F 4 "399-17911-1-ND" H 3750 2150 50  0001 C CNN "DigiKey"
	1    3750 2150
	1    0    0    -1  
$EndComp
Wire Wire Line
	3500 2250 3750 2250
Wire Wire Line
	3750 2250 4250 2250
Connection ~ 3750 2250
Wire Wire Line
	3750 2050 3500 2050
Wire Wire Line
	3500 2150 3600 2150
Wire Wire Line
	3600 2150 3600 1950
Wire Wire Line
	3600 1950 4250 1950
Text Notes 1200 2550 0    50   ~ 0
Note:  connector P/N is for right angle version\n
$Comp
L quad_sipm_symbols:D_Sipm_Quad D3
U 1 1 629A37BE
P 4250 1300
F 0 "D3" V 4246 912 50  0000 R CNN
F 1 "D_Sipm_Quad" V 3950 1350 50  0000 R CNN
F 2 "quad_sipm:sipm_6x6_bga" H 4200 1300 50  0001 C CNN
F 3 "~" H 4200 1300 50  0001 C CNN
F 4 "S14160-6050HS" H 4250 1300 50  0001 C CNN "MfgNo"
	1    4250 1300
	0    -1   -1   0   
$EndComp
Wire Wire Line
	4250 1200 4350 1200
Connection ~ 4250 1200
Connection ~ 4350 1200
Wire Wire Line
	4350 1200 4450 1200
Connection ~ 4450 1200
Wire Wire Line
	4450 1200 4550 1200
Wire Wire Line
	4250 1500 4350 1500
Connection ~ 4250 1500
Connection ~ 4350 1500
Wire Wire Line
	4350 1500 4450 1500
Connection ~ 4450 1500
Wire Wire Line
	4450 1500 4550 1500
$Comp
L quad_sipm_symbols:D_Sipm_Quad D4
U 1 1 629A5528
P 4250 2050
F 0 "D4" V 4246 1662 50  0000 R CNN
F 1 "D_Sipm_Quad" V 3950 2100 50  0000 R CNN
F 2 "quad_sipm:sipm_6x6_bga" H 4200 2050 50  0001 C CNN
F 3 "~" H 4200 2050 50  0001 C CNN
F 4 "S14160-6050HS" H 4250 2050 50  0001 C CNN "MfgNo"
	1    4250 2050
	0    -1   -1   0   
$EndComp
Wire Wire Line
	4250 1950 4350 1950
Connection ~ 4250 1950
Connection ~ 4350 1950
Wire Wire Line
	4350 1950 4450 1950
Connection ~ 4450 1950
Wire Wire Line
	4450 1950 4550 1950
Wire Wire Line
	4250 2250 4350 2250
Connection ~ 4250 2250
Connection ~ 4350 2250
Wire Wire Line
	4350 2250 4450 2250
Connection ~ 4450 2250
Wire Wire Line
	4450 2250 4550 2250
$Comp
L quad_sipm_symbols:D_Sipm_Quad D1
U 1 1 629A6BAF
P 2500 1300
F 0 "D1" V 2496 912 50  0000 R CNN
F 1 "D_Sipm_Quad" V 2200 1350 50  0000 R CNN
F 2 "quad_sipm:sipm_6x6_bga" H 2450 1300 50  0001 C CNN
F 3 "~" H 2450 1300 50  0001 C CNN
F 4 "S14160-6050HS" V 2100 1100 50  0000 C CNN "MfgNo"
	1    2500 1300
	0    -1   -1   0   
$EndComp
Wire Wire Line
	2500 1200 2600 1200
Connection ~ 2500 1200
Connection ~ 2600 1200
Wire Wire Line
	2600 1200 2700 1200
Connection ~ 2700 1200
Wire Wire Line
	2700 1200 2800 1200
Wire Wire Line
	2500 1500 2600 1500
Connection ~ 2500 1500
Connection ~ 2600 1500
Wire Wire Line
	2600 1500 2700 1500
Connection ~ 2700 1500
Wire Wire Line
	2700 1500 2800 1500
$Comp
L quad_sipm_symbols:D_Sipm_Quad D2
U 1 1 629A8647
P 2500 2050
F 0 "D2" V 2496 1662 50  0000 R CNN
F 1 "D_Sipm_Quad" V 2200 2100 50  0000 R CNN
F 2 "quad_sipm:sipm_6x6_bga" H 2450 2050 50  0001 C CNN
F 3 "~" H 2450 2050 50  0001 C CNN
F 4 "S14160-6050HS" H 2500 2050 50  0001 C CNN "MfgNo"
	1    2500 2050
	0    -1   -1   0   
$EndComp
Connection ~ 2600 1950
Wire Wire Line
	2600 1950 2700 1950
Connection ~ 2700 1950
Wire Wire Line
	2700 1950 2800 1950
Wire Wire Line
	2500 1950 2600 1950
Connection ~ 2500 1950
Wire Wire Line
	2500 2250 2600 2250
Connection ~ 2500 2250
Connection ~ 2600 2250
Wire Wire Line
	2600 2250 2700 2250
Connection ~ 2700 2250
Wire Wire Line
	2700 2250 2800 2250
$Comp
L Mechanical:MountingHole H1
U 1 1 629973DC
P 1500 3000
F 0 "H1" H 1500 3200 50  0000 C CNN
F 1 "MountingHole" H 1500 3125 50  0000 C CNN
F 2 "quad_sipm:MountingHole_6.4_pad" H 1500 3000 50  0001 C CNN
F 3 "~" H 1500 3000 50  0001 C CNN
	1    1500 3000
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H3
U 1 1 62997555
P 2000 3000
F 0 "H3" H 2000 3200 50  0000 C CNN
F 1 "MountingHole" H 2000 3125 50  0000 C CNN
F 2 "quad_sipm:MountingHole_6.4_pad" H 2000 3000 50  0001 C CNN
F 3 "~" H 2000 3000 50  0001 C CNN
	1    2000 3000
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H4
U 1 1 629977C0
P 2500 3000
F 0 "H4" H 2500 3200 50  0000 C CNN
F 1 "MountingHole" H 2500 3125 50  0000 C CNN
F 2 "quad_sipm:MountingHole_6.4_pad" H 2500 3000 50  0001 C CNN
F 3 "~" H 2500 3000 50  0001 C CNN
	1    2500 3000
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H5
U 1 1 62997B2E
P 3000 3000
F 0 "H5" H 3000 3200 50  0000 C CNN
F 1 "MountingHole" H 3000 3125 50  0000 C CNN
F 2 "quad_sipm:MountingHole_6.4_pad" H 3000 3000 50  0001 C CNN
F 3 "~" H 3000 3000 50  0001 C CNN
	1    3000 3000
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H2
U 1 1 62997D7A
P 1500 3500
F 0 "H2" H 1500 3700 50  0000 C CNN
F 1 "MountingHole" H 1500 3625 50  0000 C CNN
F 2 "quad_sipm:MountingHole_8.4mm_M8_Pad" H 1500 3500 50  0001 C CNN
F 3 "~" H 1500 3500 50  0001 C CNN
	1    1500 3500
	1    0    0    -1  
$EndComp
$EndSCHEMATC
